package com.monkhub.linuxterminal;

import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Details extends AppCompatActivity {
    private List<String> Name;
    private List<String> details;
    RecyclerView recyclerView;
    private DetailAdapter detailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.recyclerview);
        Name = new ArrayList<>();
        details = new ArrayList<>();
        readFromFile();
    }
    private String readFromFile() {

        String ret = "";

        try {
            FileInputStream inputStream = new FileInputStream(Environment.getExternalStorageDirectory() + "/monkhub/" + "details.json");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
                //Log.d("ret",ret);
                try {
                    //String jsonLocation = AssetJSONFile("formules.json", context);
                    JSONObject jsonobject = new JSONObject(ret);
                    //Log.d("ret",jsonobject.toString());
                    JSONArray jarray = jsonobject.getJSONArray("cast");
                   // Log.d("array", jarray.toString());
                    for (int i = 0; i < jarray.length(); i++) {
                        JSONObject jb = (JSONObject) jarray.get(i);
                        String mTitle = jb.getString("name");
                        Log.d("name",mTitle);
                        String mRating = jb.getString("detail");

                        Name.add(mTitle);
                        details.add(mRating);
                        // Log.d(mTitle, mRating);
                    }
                    detailAdapter = new DetailAdapter(this, Name,details);
                    recyclerView.setAdapter(detailAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}
