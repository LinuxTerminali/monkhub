package com.monkhub.linuxterminal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Home extends AppCompatActivity {
    private ViewAdapter viewAdapter;
    private List<String> url;
    private List<String> movie;
    private List<String> rating;
    private List<String> id;

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recyclerview);
        url = new ArrayList<>();
        movie = new ArrayList<>();
        rating = new ArrayList<>();
        id = new ArrayList<>();
        readFromFile();
        if (!connectivityStatus(Home.this)) {
            Snackbar.make(findViewById(R.id.parentView), "Please connect to Internet to show images", Snackbar.LENGTH_LONG)
                    .show();

        }

    }
    private String readFromFile() {

        String ret = "";

        try {
            FileInputStream inputStream = new FileInputStream(Environment.getExternalStorageDirectory() + "/monkhub/" + "movie.json");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
                try {
                    //String jsonLocation = AssetJSONFile("formules.json", context);
                    JSONObject jsonobject = new JSONObject(ret);
                    JSONArray jarray = jsonobject.getJSONArray("results");
                    //Log.d("array", jarray.toString());
                    for (int i = 0; i < jarray.length(); i++) {
                        JSONObject jb = (JSONObject) jarray.get(i);
                        String mTitle = jb.getString("title");
                        String mRating = jb.getString("vote_average");
                        String mUrl = jb.getString("poster_path");
                        String mId = jb.getString("id");
                        movie.add(mTitle);
                        rating.add(mRating);
                        url.add(mUrl);
                        id.add(mId);
                    }
                    viewAdapter = new ViewAdapter(this, url,movie,rating,id);
                    recyclerView.setAdapter(viewAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }


    /*method for testing connectivity status*/
    private static boolean connectivityStatus(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

    }
}
