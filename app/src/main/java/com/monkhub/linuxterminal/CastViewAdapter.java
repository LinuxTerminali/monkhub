package com.monkhub.linuxterminal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

/**
 * Created by linux on 1/4/18.
 */

public class CastViewAdapter  extends RecyclerView.Adapter<CastViewAdapter.VideoHolder> {

    private final Context mContext;
    private final List<String> urls;
    private final List<String> oName;
    private final List<String> mName;


    public CastViewAdapter(Context mContext, List<String> urls,List<String> oName,List<String> mName) {
        this.mContext = mContext;
        this.urls = urls;
        this.oName = oName;
        this.mName = mName;

    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        VideoHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v1 = inflater.inflate(R.layout.cast_adapter, parent, false);
        viewHolder = new VideoHolder(v1);

        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final VideoHolder holder, final int position) {
        final String url = urls.get(position);

        holder.title.setText(oName.get(position));
        holder.mName.setText(mName.get(position));
        String base_image_url ="https://image.tmdb.org/t/p/w150";

        String image = base_image_url+url;
        //Log.d("url",image);
        Picasso.with(mContext)
                .load(image)
                .error(R.drawable.thumbnail)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        holder.thumbnail.setImageBitmap(bitmap);
                        Palette.from(bitmap)
                                .generate(new Palette.PaletteAsyncListener() {
                                    @Override
                                    public void onGenerated(Palette palette) {
                                        Palette.Swatch textSwatch = palette.getVibrantSwatch();
                                        if (textSwatch == null) {
                                            //Toast.makeText(mContext, "Null swatch :(", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        holder.cardView.setBackgroundColor(textSwatch.getRgb());
                                        //Log.d(oName.get(position),textSwatch.getRgb()+"");
                                        holder.title.setTextColor(textSwatch.getTitleTextColor());
                                        holder.mName.setTextColor(textSwatch.getBodyTextColor());
                                    }
                                });
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, Details.class);
                //intent.putExtra("id",id.get(position));
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return urls.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView mName;
        public final CardView cardView;
        public final ImageView thumbnail;


        public VideoHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.oname);
            mName = itemView.findViewById(R.id.mname);
            cardView = itemView.findViewById(R.id.card_view);
            thumbnail = itemView.findViewById(R.id.thumbnail);

        }


    }

}
