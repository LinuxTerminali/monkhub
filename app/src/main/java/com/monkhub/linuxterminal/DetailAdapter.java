package com.monkhub.linuxterminal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

/**
 * Created by linux on 1/4/18.
 */

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.VideoHolder> {

    private final Context mContext;
    private final List<String> Name;
    private final List<String> detail;


    public DetailAdapter(Context mContext, List<String> Name, List<String> detail) {
        this.mContext = mContext;
        this.Name = Name;
        this.detail = detail;

    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        VideoHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v1 = inflater.inflate(R.layout.detail_adapter, parent, false);
        viewHolder = new VideoHolder(v1);

        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final VideoHolder holder, final int position) {

        holder.name.setText(Name.get(position));
        holder.mdetail.setText(detail.get(position));
       // holder.mdetail.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);


        //Log.d("url",image);



    }

    @Override
    public int getItemCount() {
        return Name.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        public final TextView name;
        public final TextView mdetail;
        public final CardView cardView;


        public VideoHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.detailname);
            mdetail = itemView.findViewById(R.id.mdetail);
            cardView = itemView.findViewById(R.id.card_view);

        }


    }

}
