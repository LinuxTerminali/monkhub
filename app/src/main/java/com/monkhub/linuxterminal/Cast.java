package com.monkhub.linuxterminal;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Cast extends AppCompatActivity {
    private CastViewAdapter viewAdapter;
    private List<String> url;
    private List<String> oName;
    private List<String> mName;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cast);
        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        String movie = intent.getStringExtra("movie");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(movie+" Cast");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.recyclerview);
        url = new ArrayList<>();
        oName = new ArrayList<>();
        mName = new ArrayList<>();
        readFromFile(id);
    }


    private String readFromFile(String id) {

        String ret = "";

        try {
            FileInputStream inputStream = new FileInputStream(Environment.getExternalStorageDirectory() + "/monkhub/" + id+".json");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
                //Log.d("ret",ret);
                try {
                    //String jsonLocation = AssetJSONFile("formules.json", context);
                    JSONObject jsonobject = new JSONObject(ret);
                    //Log.d("ret",jsonobject.toString());
                    JSONArray jarray = jsonobject.getJSONArray("cast");
                    //Log.d("array", jarray.toString());
                    for (int i = 0; i < jarray.length(); i++) {
                        JSONObject jb = (JSONObject) jarray.get(i);
                        String oriName = jb.getString("name");
                        String mcharacter = jb.getString("character");
                        String mUrl = jb.getString("profile_path");
                        //String mId = jb.getString("id");
                        oName.add(oriName);
                        mName.add(mcharacter);
                        url.add(mUrl);
                        //id.add(mId);
                        // Log.d(mTitle, mRating);
                    }
                    viewAdapter = new CastViewAdapter(this, url,oName,mName);
                    recyclerView.setAdapter(viewAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

}
